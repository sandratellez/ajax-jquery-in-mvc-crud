﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models.Modelos;
namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {

        FnConsultar data = new FnConsultar();
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpGet]
        public JsonResult GetLista(int x)
        {
            var lista = data.querypersonas(x);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult queryinsert(string nombre, string sexo, int edad, string observaciones)
        {
            var bandera = data.queryinsert(nombre,sexo,edad,observaciones);
            return Json(bandera);
        }

        [HttpPost]
        public JsonResult querydelete(string id)
        {
            var bandera = data.querydelete(id);
            return Json(bandera);
        }

        [HttpPut]
        public JsonResult queryupdate(int id,string nombre, string sexo, int edad, string observaciones)
        {
            var bandera = data.queryupdate(id,nombre, sexo, edad, observaciones);
            return Json(bandera);
        }

    }
}
