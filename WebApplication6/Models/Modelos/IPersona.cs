﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models.Modelos
{
    public class IPersona
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string sexo { get; set; }
        public int edad { get; set; }
        public string oberservacion { get; set; }

    }
}