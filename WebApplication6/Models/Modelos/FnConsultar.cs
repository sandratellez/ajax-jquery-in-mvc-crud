﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication6.Models.Modelos
{
    public class FnConsultar
    {

        DataAccess data = new DataAccess();

        public List<IPersona> querypersonas(int x)
        {
            List<IPersona> lista = new List<IPersona>();

            try
            {

                DataSet ds = new DataSet();

                SqlParameter[] param = new SqlParameter[]
                {
                    data.takeparam("@ID_CLIENTE",SqlDbType.Int,0,1),
                };

                ds = data.execData("[dbo].[SP_BUSCAR_CLIENTE]",param,1);

                if (ds!= null)
                {
                    if (ds.Tables[0].Rows.Count>0)
                    {

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            IPersona p = new IPersona();

                            p.id = int.Parse(dr[0].ToString());
                            p.nombre = dr[1].ToString();
                            p.sexo = dr[2].ToString();
                            p.edad = int.Parse(dr[3].ToString());
                            p.oberservacion = dr[4].ToString();
                            lista.Add(p);
                        }

                    }

                }
                return lista;

            }
            catch (Exception ex)
            {

                return lista;
            }

        }



        public string queryinsert (string nombre,string sexo, int edad, string observaciones)
        {
            string identi="";

            try
            {

                DataSet ds = new DataSet();

                SqlParameter[] param = new SqlParameter[]
                {
                    data.takeparam("@NOMBRE_C",SqlDbType.NVarChar,100,nombre),
                    data.takeparam("@SEXO",SqlDbType.NChar,1,sexo),
                    data.takeparam("@EDAD",SqlDbType.Int,0,edad),
                    data.takeparam("@OBSERVACIONES",SqlDbType.NVarChar,100,observaciones),
                };

                ds = data.execData("[dbo].[SP_INSERTAR_CLIENTE]", param, 1);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        
                        identi= ds.Tables[0].Rows[0][0].ToString();

                    }

                }
                return identi;

            }
            catch (Exception ex )
            {

                return identi;
            }

        }


        public string querydelete(string id)
        {
            string identi = "";

            try
            {

                DataSet ds = new DataSet();

                SqlParameter[] param = new SqlParameter[]
                {
                    data.takeparam("@ID_CLIENTE",SqlDbType.Int,0,id),
                  
                };

                ds = data.execData("[dbo].[SP_ELIMINAR_CLIENTE]", param, 1);
                return "1";

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        identi = ds.Tables[0].Rows[0][0].ToString();

                    }

                }
               

            }
            catch (Exception)
            {

                return identi;
            }

        }


        public string queryupdate(int id,string nombre, string sexo, int edad, string observaciones)
        {
            string identi = "";

            try
            {

                DataSet ds = new DataSet();

                SqlParameter[] param = new SqlParameter[]
                {
                    data.takeparam("@ID_CLIENTE",SqlDbType.Int,0,id),
                    data.takeparam("@NOMBRE_C",SqlDbType.NVarChar,100,nombre),
                    data.takeparam("@SEXO",SqlDbType.NChar,1,sexo),
                    data.takeparam("@EDAD",SqlDbType.Int,0,edad),
                    data.takeparam("@OBSERVACIONES",SqlDbType.NVarChar,100,observaciones),
                };

                ds = data.execData("[dbo].[SP_MODIFICAR_CLIENTE]", param, 1);
                if (ds==null)
                {
                    return "";
                }
                else return "1";
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        identi = ds.Tables[0].Rows[0][0].ToString();

                    }

                }
                

            }
            catch (Exception ex)
            {

                return identi;
            }

        }




    }
}